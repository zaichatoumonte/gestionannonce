package projetannonce

import grails.plugin.springsecurity.annotation.Secured
import grails.converters.JSON
import grails.validation.ValidationException
import grails.plugin.springsecurity.SpringSecurityService


@Secured(['ROLE_ADMIN','ROLE_MODO','ROLE_USER'])
class AnnoncesController {

    AnnoncesService annoncesService
    UploadService uploadService
    UserService userService
    SpringSecurityService springSecurityService

    static allowedMethods = [
            searchAnnonce:"GET",
            updateAnnonce: "PUT",
            deleteAnnonce: "DELETE",
            createAnnonce: "POST",
    ]

    //affiche la liste des annonces et effectue la gestion
    @Secured(['ROLE_ADMIN','ROLE_MODO'])
    def index() {
        respond annoncesService.list(null),model:[userList: userService.list(null)]
    }

    //affiche uniquement la liste des annonces cree par l utilisateur connecte
    def userAuthentificatedAnnonces(){
        User user = userService.get(springSecurityService.currentUser.id)
        [user:user]
    }

    def getAnnonce(Long id_annonce){
        Map map=[:]
        try {
            Annonce annonce=annoncesService.get(id_annonce);
            if(annonce==null){
                map.status=false
                map.message="Annonce inexistante!!!"
                render map as JSON
            }
            else{
                map.status=true
                map.data=[id:annonce.id, titre:annonce.titre, desc:annonce.description,prix:annonce.prix]
                render map as JSON
            }
        }
        catch (Exception e){
            map.status=false
            map.message=e.message
            map.stack=e.stackTrace.toArrayString()
            render map as JSON
        }
    }

    def createAnnonce(Annonce annonce, Long auteur_id){
        Map map=[:]
        map.status=false
        try {
            //Long userId=1
            User userInstance=userService.get(auteur_id)
            if(userInstance==null){
                throw new Exception("Autheur inexistant!!!")
            }
            annonce.setAutheur(userInstance)

            def uploadFile = request.getFile('NewIll') // image is the parameter name
            if(uploadFile==null){
                map.message="NO Illustration file Defined"
                render map as JSON
            }
            def imageBytes = uploadFile.getBytes()
            def size=uploadFile.getSize()
            def illChemin=uploadService.doUpload(imageBytes)

            Illustration ill=new Illustration(tailleFichier: size, nomFichier: illChemin)
            annonce.addToIllustrations(ill)
            annonce.save(flush:true)

            map.status=true
            map.data=[
                    id:annonce.id,
                    prix: annonce.prix,
                    desc:annonce.description,
                    titre:annonce.titre,
                    chemin_fichier:grailsApplication.config.assets.url + illChemin,
                    autheur:userInstance.username,
                    nbIll:1
            ]
        }
        catch(Exception e){
            map.message=e.getMessage()
        }
        finally{
            render map as JSON
        }
    }

    def updateAnnonce(Annonce annonce){
        Map map=[:]
        if(annonce==null){
            map.status=false
            map.message="Parametre de modification non fournit"
            render map as JSON
        }
        else{
            try {
                annonce.save(flush:true)
                map.status=true
            }
            catch (ValidationException e) {
                map.status=false
                map.errors=annonce.errors.allErrors.toArray()
                map.message="Erreur de validation, donnee non conforme"
            }
            catch(Exception e){
                map.status=false
                map.message=e.getMessage()
            }
            finally {
                render map as JSON
            }
        }
    }

    def deleteAnnonce(Long annonce_id){
        Map map=[:]
        map.status=false
        try {
            if(annonce_id==null){
                map.message="L'annonce a supprimer n'a pas ete identifie"
            }
            else{
                annoncesService.delete(annonce_id)
                map.status=true
            }
        }
        catch (Exception e){
            map.message=e.getMessage()
        }
        finally{
            render map as JSON
        }
    }

}
