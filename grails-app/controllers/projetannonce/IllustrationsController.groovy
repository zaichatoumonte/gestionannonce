package projetannonce

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN','ROLE_MODO','ROLE_USER'])
class IllustrationsController {

    AnnoncesService annoncesService
    IllustrationService illustrationService
    UploadService uploadService

    static allowedMethods = [
            updateIllustration: ["PUT","POST"],
            deleteIllustration: "DELETE",
            addIllustration: "POST",
    ]

    //affiche les illustrations liee a une annonce
    def index(Long idAnnonce) {
        def annonce=annoncesService.get(idAnnonce)
        if(annonce==null)render(status: 404, text: 'Annonce non identifie')
        respond annonce
    }

    //retourne les informations sur une illustration connaissant son id et l id de l annonce auxquels il est liee
    def getIllustration(Long id_illustration, Long id_annonce){
        Map map=[:]
        map.status=false;
        try {
            Illustration illustration=illustrationService.get(id_illustration)
            if(illustration==null || !illustration.controlAnonceId(id_annonce)){
                map.message="Illustration inexistante!!!"
            }
            else{
                map.status=true
                map.nomFichier=illustration.nomFichier
                map.id_illustration=illustration.id
                map.id_annonce=illustration.annonceId
            }
        }
        catch( Exception e){
            map.message=e.getMessage()
        }
        finally{
            render map as JSON
        }
    }

    def updateIllustration(Long id_illustration, Long id_annonce ){
        Map map=[:]
        map.status=false;
        try {
            Illustration illustration=illustrationService.get(id_illustration)
            if(illustration==null || !illustration.controlAnonceId(id_annonce)){
                map.message="Illustration inexistante!!!"
            }
            else{
                def uploadFile = request.getFile('NewIll')
                if(uploadFile==null || uploadFile.isEmpty()){
                    map.message="NO Illustration file Defined"
                    render map as JSON
                }
                def imageBytes = uploadFile.getBytes()
                def size=uploadFile.getSize()
                def illChemin=uploadService.doUpload(imageBytes)

                illustration.nomFichier=illChemin
                illustration.tailleFichier=size
                illustrationService.save(illustration)
                map.status=true
            }
        }
        catch (Exception e){
            map.message=e.getMessage()
        }
        finally {
            render map as JSON
        }

    }

    def addIllustration(Long id_annonce){
        Map map=[:]
        map.status=false;
        try {

            Annonce annonce=annoncesService.get(id_annonce);
            if(annonce==null){
                throw new Exception("Annonce inexistante. Chaque illustration doit etre reliee a une annonce.")
            }

            def uploadFile = request.getFile('NewIll')
            if(uploadFile==null || uploadFile.isEmpty()){
                throw new Exception("Illustration image not defined or empty")
            }
            def imageBytes = uploadFile.getBytes()
            def size=uploadFile.getSize()
            def illChemin=uploadService.doUpload(imageBytes)

            Illustration ill=new Illustration(nomFichier: illChemin,tailleFichier: size)
            ill.setAnnonce(annonce)
            ill.save(flush:true);
            map.status=true
            map.data=[
                    chemin_fichier:grailsApplication.config.assets.url + illChemin,
                    id:ill.id
            ]
        }
        catch (Exception e){
            map.message=e.getMessage()
        }
        finally {
            render map as JSON
        }

    }

    def deleteIllustration(Long illustration_id){
        Map map=[:]
        map.status=false
        try {
            if(illustration_id==null){
                map.message="L'illustration a supprimer n'a pas ete identifie"
            }
            else{
                illustrationService.delete(illustration_id)
                map.status=true
            }
        }
        catch (Exception e){
            map.message=e.getMessage()
        }
        finally{
            render map as JSON
        }
    }

    /*def updateIllustration(Long id_illustration, Long id_annonce ){

        def uploadFile = request.getFile('NewIll') // image is the parameter name
        def imageBytes = uploadFile.getBytes()
        def orifilename = uploadFile.getOriginalFilename()
        def filesize=uploadFile.size
        def webRootDir = servletContext.getRealPath("/")

        String Message=""
        def homeDir = new File(System.getProperty("user.home"))
        def homeurl = "Documents/Upload/"
        File fileDest = new File(homeDir,homeurl+orifilename)
        if(fileDest.exists())
            uploadFile.transferTo(fileDest)
        else{
            fileDest.createNewFile()
            uploadFile.transferTo(fileDest)
        }
        render "Uploaded Successfully"
    }*/
}
