package projetannonce

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException

@Secured(['ROLE_ADMIN'])
class UsersController {

    //def user = springSecurityService.currentUser current user
    //springSecurityService.loadCurrentUser() si pas  besoin des fonction du domaine

    UserService userService
    SpringSecurityService springSecurityService
    static allowedMethods = [createUser: "POST", updateUser: "PUT", deleteUser: "DELETE"]

    @Secured(['ROLE_ADMIN','ROLE_MODO'])
    def index() {
        respond userService.list(null), model: [Rolelist:Role.list()]
    }

    @Secured(['ROLE_ADMIN','ROLE_MODO'])
    def getUser(Long userId){
        Map map=[:]
        if(userId==null)throw new Exception("Parametre non fournit")
        try {
            User user=userService.get(userId);
            if(user==null) throw new Exception("Utilisateur inexistant!!!");
            def userRole=user.getAuthorities()

            Map data=[:]
            if(userRole.size()>0){
                data.role_id=userRole[0].id
                data.role_name=userRole[0].authority
            }
            data.id=user.id
            data.username=user.username

            map.data=data
            map.status=true
        }
        catch (Exception e){
            map.status=false
            map.message=e.message
        }
        finally {
            render map as JSON
        }
    }

    def createUser(User user,Long role_id){
        Map map=[:]
        map.status=false
        try {
            if(user==null)throw new Exception("Vous n'avez pas fourni les informations sur l'utilisateur a creer")

            def role=Role.get(role_id)
            if(role==null)throw new Exception("Le role definit pour l'utilisateur n'existe pas")

            def userSaved = user.save(flush:true)
            UserRole.create(userSaved, role, true)

            def userSavedAuth=userSaved.getAuthorities().first()
            map.data=[
                    id:userSaved.id,
                    username:userSaved.username,
                    role_id:userSavedAuth.id,
                    role_name:userSavedAuth.authority,
            ]
            map.status=true
        }
        catch(ValidationException e){
            map.message="Le Nom d'utilisateur est peut deja utiliser. Veuillez le modifier svp"
            map.errors=e.errors
        }
        catch(Exception e){
            map.message=e.getMessage()
        }
        finally{
            render map as JSON
        }
    }

    def updateUser(Long id, String username , String password, Long id_role){
        Map map=[:]
        map.status=false
        try {
            //recuperation du nouveau role
            if(id==null || id_role==null )throw new Exception("Parametre de modification non fournit")

            //recuperation des ancienne informations sur l utilisateur
            //et de l existence du nouveau role
            def user=userService.get(id)
            def role=Role.get(id_role)

            if(user==null || role==null)throw new Exception("Information fournit non valide")


            if(user.username!=username){
                user.setUsername(username)
            }
            //
            if( password!="" && !passwordEncoder.isPasswordValid(user.password, password, null)){
                user.setPassword(password)
            }

            user.save(flush:true)
            UserRole.removeAll(user)

            UserRole.create(user,role,true)

            map.data=[
                    id:user.id,
                    username:user.username,
                    role_id:role.id,
                    role_name:role.authority,
            ]
            map.status=true
        }
        catch(ValidationException e){
            map.message=e.getMessage()
            map.errors=e.errors
        }
        catch(Exception e){
            map.message=e.getMessage()
        }
        finally {
            render map as JSON
        }
    }

    def deleteUser(Long user_id){
        Map map=[:]
        map.status=false

        try {
            User user=userService.get(user_id)
            if(user==null)throw new Exception("Utilisateur non existant")
            //il ne faut pas supprimer tous les admins du systeme
            Role roleAdmin=Role.findByAuthority("ROLE_ADMIN")
            if(user.getAuthorities().contains(roleAdmin) && UserRole.findAllByRole(roleAdmin).size()<=1){
                throw new Exception("Impossible de supprimer cet utilisateur.")
            }
            UserRole.removeAll(user)
            userService.delete(user_id);
            map.status=true
        }
        catch (Exception e){
            map.message=e.getMessage()
        }
        finally{
            render map as JSON
        }
    }

}