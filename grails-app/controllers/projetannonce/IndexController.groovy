package projetannonce

import grails.plugin.springsecurity.annotation.Secured

class IndexController {
    AnnoncesService annoncesService

    def index() {
        def three_last_annonces = Annonce.list(max: null, sort: "dateCreated", order: "desc")
        println(three_last_annonces)
        render( view:'/index', model: [data: three_last_annonces])
    }

    def show(Long id) {
        respond annoncesService.get(id)
    }

    def searchAnnonce(){
        def req = params.mots_cles.trim()
        def res;
        if (req==null || req==""){
            res=Annonce.list()
        }
        else res = Annonce.findAllByTitreIlikeOrDescriptionIlike("%${req}%", "%${req}%")
        render(view: "search_results", model: [results: res])
    }
}
