<!doctype html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <title>Mes Annonces</title>
    <!-- DataTables -->
    <asset:stylesheet href="/datatables-bs4/css/dataTables.bootstrap4.min.css"/>
    <asset:stylesheet href="/datatables-responsive/css/responsive.bootstrap4.min.css"/>
    <asset:stylesheet href="/datatables-buttons/css/buttons.bootstrap4.min.css"/>
</head>
<body>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Mes Annonces</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Administration</a></li>
                        <li class="breadcrumb-item active">Mes Annonces</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Liste de mes annonces</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Titre</th>
                                    <th>Description</th>
                                    <th>Prix</th>
                                    <th>Une illustration</th>
                                    <th>Autheur</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <g:each in="${user.annonces}" var="annonce">
                                    <tr>
                                        <td>${annonce.titre}</td>
                                        <td>${annonce.description}</td>
                                        <td>${annonce.prix}</td>
                                        <td>
                                            <a class="nav-link" data-toggle="dropdown" href="#">
                                                <img src="${grailsApplication.config.assets.url + annonce.illustrations.first().nomFichier}" alt="illustration" width="50px" height="50px"/>
                                            </a>
                                        </td>
                                        <td>${annonce.autheur.username}</td>
                                        <td>
                                            <g:link controller="illustrations" action="index" params="[idAnnonce:annonce.id]" class="btn btn-outline-info btn-flat" title="Editer les Illustrations" data-id="${annonce.id}">
                                                <i class="fa fa-images"></i>
                                                <span class="badge badge-warning navbar-badge">${annonce.getIllustrationCount()}</span>
                                            </g:link>
                                            <button type="button" class="btn btn-outline-warning btn-flat btn_mod_ann" title="Editer l'annonce" data-id="${annonce.id}" data-toggle="modal" data-target="#modal_edit"><i class="fa fa-edit"></i></button>
                                            <button type="button" class="btn btn-outline-danger btn-flat btn_sup_ann" title="Supprimer l'annonce" data-id="${annonce.id}" data-title="${annonce.titre}"><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                </g:each>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <!-- /.col -->
            </div>
        </div>
    </section>
    <!-- /.content -->

    <!-- /.modal -->
    <div class="modal fade" id="modal_add">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Ajouter une annonce</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="ExitAdding()">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form name="modAnnForm" id="addAnnForm" method="post">
                        <p id="addErrorMessage" class="text-danger"></p>
                        <div class="form-group">
                            <label for="addAnnTitre">Titre</label>
                            <input type="text" class="form-control" name="addAnnTitre\" id="addAnnTitre" placeholder="Titre de l'annonce">
                        </div>
                        <div class="form-group">
                            <label for="addAnnPrix">Prix (Euro)</label>
                            <input type="number" class="form-control" min="0" step="0.01" name="addAnnPrix"  id="addAnnPrix" placeholder="Prix de l'annonce">
                        </div>
                        <div class="form-group">
                            <label for="addAnnDesc">Description</label>
                            <textarea class="form-control" rows="3" placeholder="Description ..." name="addAnnDesc"  id="addAnnDesc"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="addAnnAutheur">Autheur</label>
                            <select class="form-control" id="addAnnAutheur" name="addAnnAutheur">
                                <option value="${user.id}">${user.username}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="addAnnIll">Une Illustration</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <label class="" for="addAnnIll"></label>
                                    <input type="file" class="" id="addAnnIll" name="addAnnIll">
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="ExitAdding();">Annuler</button>
                    <button type="button" class="btn btn-primary" id="btnAnnVAlidAdd" form="modAnnForm">Ajouter</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal_edit">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Editer une annonce</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form name="modAnnForm" id="modAnnForm" method="post">
                        <p id="errorMessage" class="text-danger"></p>
                        <input type="hidden" name="modAnnId" id="modAnnId"/>
                        <div class="form-group">
                            <label for="modAnnTitre">Titre</label>
                            <input type="text" class="form-control" name="modAnnTitre" id="modAnnTitre" placeholder="Titre de l'annonce">
                        </div>
                        <div class="form-group">
                            <label for="modAnnPrix">Prix (Euro)</label>
                            <input type="number" class="form-control" min="0" step="0.01" name="modAnnPrix"  id="modAnnPrix" placeholder="Prix de l'annonce">
                        </div>
                        <div class="form-group">
                            <label for="modAnnDesc">Description</label>
                            <textarea class="form-control" rows="3" placeholder="Description ..." name="modAnnDesc"  id="modAnnDesc" ></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                    <button type="button" class="btn btn-primary" id="btnAnnVAlidMod" form="modAnnForm">Modifier</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- /.content-wrapper -->
<content tag="javascript">
    <asset:javascript src="/datatables/jquery.dataTables.min.js"/>
    <asset:javascript src="/datatables-bs4/js/dataTables.bootstrap4.min.js"/>
    <asset:javascript src="/datatables-responsive/js/dataTables.responsive.min.js"/>
    <asset:javascript src="/datatables-responsive/js/responsive.bootstrap4.min.js"/>
    <asset:javascript src="/datatables-buttons/js/dataTables.buttons.min.js"/>
    <asset:javascript src="/datatables-buttons/js/buttons.bootstrap4.min.js"/>
    <asset:javascript src="/jszip/jszip.min.js"/>
    <asset:javascript src="/pdfmake/pdfmake.min.js"/>
    <asset:javascript src="/pdfmake/vfs_fonts.js"/>
    <asset:javascript src="/datatables-buttons/js/buttons.html5.min.js"/>
    <asset:javascript src="/datatables-buttons/js/buttons.print.min.js"/>
    <asset:javascript src="/datatables-buttons/js/buttons.colVis.min.js"/>

    <script>

        $(document).ready(function() {

            const url_get_annonce="<g:createLink controller="annonces" action="getAnnonce"/>";
            const url_update_annonce="<g:createLink controller="annonces" action="updateAnnonce"/>";
            const url_delete_annonce="<g:createLink controller="annonces" action="deleteAnnonce"/>";
            const url_add_annonce="<g:createLink controller="annonces" action="createAnnonce"/>";
            const url_edit_illustration_annonce="<g:createLink controller="illustrations" action="index" params="[idAnnonce:0]"/>";

            let stateAdding=false;
            let row_to_update;

            function ExitAdding(){
                stateAdding=false;
            }

            let datatable=$('#example1').DataTable( {
                dom: 'lrBftip',
                "responsive": true,
                buttons: [
                    {
                        class:"btn btn-info btn-flat",
                        text: '<i class="fa fa-plus"></i> Ajouter',
                        action: function ( e, dt, node, config ) {
                            if(
                                !(stateAdding &&
                                    confirm("Une action d'ajout debute n'a pas ete terminer. Voulez vous continuer cette action?")
                                )
                            ){
                                stateAdding=true
                                $("#addAnnForm")[0].reset();
                            }
                            $("#addErrorMessage").text("");
                            $("#modal_add").modal("show");
                        }
                    },
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5',
                    "print",// "colvis"
                ],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
                }
            });

            datatable.buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

            $("#btnAnnVAlidAdd").on("click",(e)=>{
                e.preventDefault();
                let titre=$("#addAnnTitre").val().trim();
                let desc=$("#addAnnDesc").val().trim();
                let prix=$("#addAnnPrix").val().trim();
                let illustrationFile=$("#addAnnIll").get(0);
                let auteur_id=$("#addAnnAutheur").val().trim();

                if(titre=="" || desc=="" || prix=="" || auteur_id==""){
                    alert("Veuillez renseigner tous les champs svp!!!")
                    $("#addErrorMessage").text("Veuillez renseigner tous les champs svp!!!");
                }
                else if(titre.length<10){
                    alert("Le titre doit avoir une longueur minimum de 10 caracteres");
                    $("#addErrorMessage").text("Le titre doit avoir une longueur minimum de 10 caracteres");
                }
                else if(prix<0){
                    alert("Le prix ne peut pas etre negatif")
                    $("#errorMessage").text("Le prix ne peut pas etre negatif");
                }
                else if(illustrationFile.files.length==0){
                    alert("Veuillez selectionner une image d'illustration svp!!!");
                    $("#addErrorMessage").text("Veuillez selectionner une image d'illustration svp!!!");
                }
                else{
                    prix=prix.replace(".",",")
                    let indexVirgule=prix.indexOf(",")
                    if(indexVirgule!=-1)prix=prix.substr(0,indexVirgule+3)

                    prix=prix.substr(0,prix.indexOf(",")+3)
                    illustrationFile=illustrationFile.files[0];
                    var jForm = new FormData();
                    jForm.append("NewIll", illustrationFile);
                    jForm.append("description", desc);
                    jForm.append("titre", titre);
                    jForm.append("prix", prix);
                    jForm.append("auteur_id", auteur_id);
                    $("#addErrorMessage").text("")
                    $.ajax({
                        url: url_add_annonce,
                        type: 'POST',
                        data:jForm,
                        mimeType: "multipart/form-data",
                        contentType: false,
                        cache: false,
                        processData: false,
                        dataType: "json",
                        success: function(result) {
                            // Do something with the result
                            if(result.status){
                                alert("Annonce cree avec succes!!!")
                                stateAdding=false;
                                //window.location.reload()
                                let ann_data=result.data
                                datatable.row.add([
                                    ann_data.titre,
                                    ann_data.desc,
                                    ann_data.prix,
                                    '<a class="nav-link" data-toggle="dropdown" href="#">'+
                                    '<img src="'+ann_data.chemin_fichier+'" alt="illustration" width="50px" height="50px"/>'+
                                    '</a>',
                                    ann_data.autheur,
                                    '<a href="'+url_edit_illustration_annonce.replace("0",ann_data.id)+'" class="btn btn-outline-info btn-flat" title="Editer les Illustrations" data-id="'+ann_data.id+'">' +
                                    '<i class="fa fa-images"></i><span class="badge badge-warning navbar-badge">'+ann_data.nbIll+'</span></a>' +
                                    '<button type="button" class="btn btn-outline-warning btn-flat btn_mod_ann" title="Editer l\'annonce" data-id="'+ann_data.id+'" data-toggle="modal" data-target="#modal_edit"><i class="fa fa-edit"></i></button>'+
                                    '<button type="button" class="btn btn-outline-danger btn-flat btn_sup_ann" title="Supprimer l\'annonce" data-id="'+ann_data.id+'" data-title="'+ann_data.titre+'"><i class="fa fa-trash"></i></button>'
                                ]).draw();
                                $("#modal_add").modal("hide")
                            }
                            else{
                                $("#addErrorMessage").text(result.message);
                                alert(result.message);
                                console.error("Erreur ",result)
                            }
                        },
                        error:function(err){
                            $("#addErrorMessage").text("une erreur est survenue");
                            alert("une erreur est survenue");
                            console.log(err)
                        }
                    });
                }
            });

            $("table").on("click",".btn_mod_ann",(e)=>{
                let id_ann=$(e.currentTarget).data("id");
                $.get(url_get_annonce,{id_annonce:id_ann}).done((res)=>{
                    if(res.status){
                        row_to_update=$(e.currentTarget).parents("tr");
                        $("#modAnnTitre").val(res.data.titre);
                        $("#modAnnDesc").val(res.data.desc);
                        $("#modAnnPrix").val(res.data.prix);
                        $("#modAnnId").val(res.data.id);
                    }
                    else{
                        alert("Erreur Survenue : "+res.message);
                    }
                }).fail((error)=>{
                    alert("Desole, une error est survenue lors du chargement des donnees!!!!");
                    console.error(error);
                })
            });

            $("#btnAnnVAlidMod").on("click",(e)=>{
                let id=$("#modAnnId").val();
                let titre=$("#modAnnTitre").val().trim();
                let desc=$("#modAnnDesc").val().trim();
                let prix=$("#modAnnPrix").val().trim();
                if(titre=="" || desc=="" || prix==""){
                    alert("Veuillez renseigner tous les champs svp!!!")
                }
                else if(titre.length<10){
                    $("#errorMessage").text("Le titre doit avoir une longueur minimum de 10 caracteres");
                }
                else if(prix<0){
                    $("#errorMessage").text("Le prix ne peut pas etre negatif");
                }
                else{
                    prix=prix.replace(".",",")
                    let indexVirgule=prix.indexOf(",")
                    if(indexVirgule!=-1)prix=prix.substr(0,indexVirgule+3)
                    $.ajax({
                        url: url_update_annonce,
                        type: 'PUT',
                        dataType: "json",
                        data:{id:id, description:desc, titre:titre, prix:prix},
                        success: function(result) {
                            // Do something with the result
                            if(result.status){
                                alert("MOdification effectuee avec succes!!!")
                                //window.location.reload()
                                let d = datatable.row(row_to_update).data();
                                d[5]=d[5].replace(d[0],titre)
                                d[0]=titre
                                d[1]=desc
                                d[2]=prix
                                datatable.row( row_to_update ).data( d ).draw();
                                row_to_update=null
                                $("#modal_edit").modal("hide")
                            }
                            else{
                                $("#errorMessage").text(result.message);
                                alert(result.message);
                                console.error("Erreur ",result.errors)
                            }
                        },
                        error:function(err){
                            $("#errorMessage").text("une erreur est survenue");
                            alert("une erreur est survenue");
                        }
                    });
                }
            });

            $("table").on("click",".btn_sup_ann",(e)=>{
                let title=$(e.currentTarget).data("title");
                let id=$(e.currentTarget).data("id");
                if(confirm("Etes-vous sur de vouloir supprimer l'annonce \n"+title)){
                    $.ajax({
                        url: url_delete_annonce+"?annonce_id="+id,
                        type: 'DELETE',
                        dataType: "json",
                        success: function(result) {
                            // Do something with the result
                            if(result.status){
                                alert("Suppression effectuee avec succes!!!")
                                datatable.row($(e.currentTarget).parents("tr")).remove().draw()

                                //window.location.reload()
                            }
                            else{
                                alert("Suppression non effectuee. Erreur :"+result.message);
                                console.error("Erreur ",result)
                            }
                        },
                        error:function(err){
                            alert("une erreur est survenue");
                            console.error("Erreur ",err)
                        }
                    });
                }
            });
        });
    </script>
</content>
</body>
</html>
