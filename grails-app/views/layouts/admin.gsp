<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>LeCoinCoin | <g:layoutTitle default="Toutes nos annonces pour Vous"/></title>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <g:layoutHead/>
    <!-- Font Awesome -->
    <asset:stylesheet href="fontawesome-free/css/all.min.css"/>
    <!-- Theme style -->
    <asset:stylesheet href="/adminlte.min.css"/>
</head>

<body class="hold-transition sidebar-mini layout-navbar-fixed layout-fixed">
    <!-- Site wrapper -->
    <div class="wrapper">

        <!--Header-->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">

            <!--Bouton reduire menu de droite-->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <!-- Navbar Search -->
                <li class="nav-item">
                    <a class="nav-link" data-widget="navbar-search" href="#" role="button">
                        <i class="fas fa-search"></i>
                    </a>
                    <div class="navbar-search-block">
                        <form class="form-inline">
                            <div class="input-group input-group-sm">
                                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                                <div class="input-group-append">
                                    <button class="btn btn-navbar" type="submit">
                                        <i class="fas fa-search"></i>
                                    </button>
                                    <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </li>
                <!--Bouton Plein ecran-->
                <li class="nav-item">
                    <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                        <i class="fas fa-expand-arrows-alt"></i>
                    </a>
                </li>
                <!--Utlisateur-->
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="fa fa-user-alt"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <span class="dropdown-item dropdown-header"><sec:loggedInUserInfo field='username'/></span>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-envelope mr-2"></i> Profil
                        </a>
                        <a href="<g:createLink action="userAuthentificatedAnnonces" controller="annonces"/>" class="dropdown-item">
                            <i class="fas fa-users mr-2"></i> Mes Annonces
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="<g:createLink controller="logout" action="index" />" class="dropdown-item">
                            <i class="fas fa-file mr-2"></i> Se Deconnecter
                        </a>
                    </div>
                </li>
            </ul>
        </nav>
        <!-- fin Header -->

        <!-- Sidebar -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="#" class="brand-link">
                <asset:image src="AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"/>
                <span class="brand-text font-weight-light">LeCoinCoin</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <asset:image src="UserImage.png" class="img-circle elevation-2" alt="User Image"/>
                    </div>
                    <div class="info">
                        <a href="#" class="d-block"><sec:loggedInUserInfo field='username'/></a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                   with font-awesome or any other icon font library -->
                        <li class="nav-item menu-open">
                            <a href="#" class="nav-link active">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    Administration
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview ">
                                <li class="nav-item">
                                    <a href="<g:createLink controller="annonces" action="userAuthentificatedAnnonces"/>" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Mes Annonces</p>
                                    </a>
                                </li>
                                <sec:ifAnyGranted roles='ROLE_ADMIN,ROLE_MODO'>
                                    <li class="nav-item">
                                        <a href="<g:createLink controller="annonces" action="index"/>" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Annonces</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="<g:createLink controller="users" action="index"/>" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Utilisateurs</p>
                                        </a>
                                    </li>
                                </sec:ifAnyGranted>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->

            </div>
            <!-- /.sidebar -->
        </aside>

        <g:layoutBody/>

        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
                <b>Version</b> 3.2.0-rc
            </div>
            <strong>Copyright &copy; 2021 - AICHA TOUMONTE & CHRIS AKA - Using <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->

    <asset:javascript src="application.js"/><!--jquery et boostrap-->
    <!-- AdminLTE App -->
    <asset:javascript src="adminlte.min.js"/>
    <!-- AdminLTE for demo purposes -->
    <asset:javascript src="demo.js"/>

    <!--inclure le contenu javascript se trouvant dans la balise
         <content tag="javascript"></content> -->
    <!-- Page specific script -->
    <g:pageProperty name="page.javascript"/>

</body>
</html>
