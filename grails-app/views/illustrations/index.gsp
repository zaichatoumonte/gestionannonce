<!doctype html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <title>Gestion des Illustrations</title>
    <!-- Toastr -->
    <asset:stylesheet src="MyCss.css"/>
</head>
<body>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 2644px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Illustrations</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Administration</a></li>
                        <li class="breadcrumb-item"><a href="#">Annonces</a></li>
                        <li class="breadcrumb-item active">Illustrations</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="callout callout-info">
                        <h5><i class="fas fa-info"></i> Annonces: <span class="text-info font-italic">${annonce.titre}</span> </h5>
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Prix</span>
                                        <span class="info-box-number">${annonce.prix}</span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-3 col-sm-6 col-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-success"><i class="far fa-flag"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Ajouté le</span>
                                        <span class="info-box-number">${annonce.getDateCreatedFormat()}</span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-3 col-sm-6 col-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-warning"><i class="far fa-copy"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Modifié le</span>
                                        <span class="info-box-number">${annonce.getDateUpdateFormat()}</span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-3 col-sm-6 col-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-danger"><i class="far fa-star"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Description</span>
                                        <span class="info-box-number toastsDefaultFull text-info font-underline changeColoronHover">
                                            Cliquez ici
                                        </span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                    <div class="card card-success">
                        <div class="card-body">
                            <div class="row" id="div_ill">
                                <div class="col-md-12 col-lg-12 col-xl-12 align-right">
                                    <a class="btn btn-app bg-info" id="btnShowModalAdd" data-toggle="modal" data-target="#modal_add_ill">
                                        <i class="fas fa-plus"></i> Nouveau
                                    </a>
                                </div>
                                <g:each in="${annonce.illustrations}" var="illustration">
                                    <div class="col-md-12 col-lg-6 col-xl-4" id="ill${illustration.id}">
                                        <div class="card mb-2 bg-gradient-dark ">
                                            <img src="${grailsApplication.config.assets.url + illustration.nomFichier}" alt="Illustration"/>
                                            <div class="d-flex"><!--card-img-overlay-->
%{--                                                <button type="button" class="btn btn-success btn-flat btn_edit_img" title="Editer" data-id="${illustration.id}"><i class="fa fa-edit"></i></button>--}%
                                                <button type="button" class="btn btn-danger btn-flat btn_delete_img" title="Supprimer" data-id="${illustration.id}"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </g:each>
                            </div>
                        </div>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <div class="modal fade" id="modal_add_ill">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Ajout d'illustration</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <g:form name="formAddIll" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="addNewIll">Illustration</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <label class="" for="addNewIll">Choisir une Image</label>
                                    <input type="file" class="" id="addNewIll" name="addNewIll">
                                </div>
                            </div>
                        </div>
                    </g:form>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="btnValidAdd" class="btn btn-primary" form="formAddIll">Enregistrer</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal-overlay">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="overlay">
                    <i class="fas fa-2x fa-sync fa-spin"></i>
                </div>
                <div class="modal-header">
                    <h4 class="modal-title">Modification d'illustration</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <g:form name="formUpdateIll" controller="illustrations" action="updateIllustration" method="post" enctype="multipart/form-data">
                        <g:hiddenField name="illustration_id_mod" id="illustration_id_mod" />
                        <div class="form-group">
                            <label>Illustration Actuelle</label>
                            <img id="img_mod_ill" width="300px" height="200px"/>
                        </div>
                        <div class="form-group">
                            <label for="NewIll">Nouvelle Illustration</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <label class="" for="NewIll">Choisir une Image</label>
                                    <input type="file" class="" id="NewIll" name="NewIll">
                                </div>
                            </div>
                        </div>
                    </g:form>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="btnValidMod" class="btn btn-primary" form="formUpdateIll">Enregistrer</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- /.content-wrapper -->
<content tag="javascript">
    <!-- Toastr -->
    <script>
        const id_annonce=${annonce.id};
        const descr_annonce="${annonce.description}";
        const url_get_illustration="${createLink(controller: "illustrations",action: "getIllustration")}"
        const url_update_illustration="${createLink(controller: "illustrations", action: "updateIllustration")}"
        const url_create_illustration="${createLink(controller: "illustrations", action: "addIllustration")}"
        const url_delete_illustration="${createLink(controller: "illustrations", action: "deleteIllustration")}"
        const url_image_base="${grailsApplication.config.assets.url}"

        $('.toastsDefaultFull').click(function() {
            alert(descr_annonce)
           /* $(this).Toasts('create', {
                body: descr_annonce,
                title: 'Description de l\'annonce',
                fixed:false,
                //subtitle: 'Subtitle',
                icon: 'fas fa-envelope fa-lg',
                //class:"toast-top-center",
                //position: 'bottomLeft',
            })*/
        });

        $("body").on("click",".btn_edit_img",function (e) {
            let id_illustration=$(e.currentTarget).data("id");
            $("#img_mod_ill").prop("src","");
            $("#illustration_id_mod").val("");
            $("#modal-overlay").modal("show");
            $("#modal-overlay .overlay").show();
            $.get(url_get_illustration,{
                id_illustration:id_illustration,
                id_annonce:id_annonce
            }).done(function(res){
                if(res.status){
                    $("#modal-overlay .overlay").hide();
                    $("#img_mod_ill").prop("src",url_image_base+res.nomFichier);
                    $("#illustration_id_mod").val(id_illustration);
                }
                else{
                    alert(res.message??"Une erreur est survenue");
                    $("#modal-overlay").modal("hide");
                }
            }).fail((error)=>{
                console.error(error)
                alert("Une erreur est survenue !!!")
                $("#modal-overlay").modal("hide");
            });
        });

        $("#btnValidMod").on("click",(e)=>{
            e.preventDefault();
            let id_ill_to_mod=$('#illustration_id_mod').val();
            let file=$('#NewIll').get(0)
            if(file==null || file.length==0){
                alert("Illustration non renseigne!!!");
            }
            else{
                file=file.files[0];
                let jForm = new FormData();
                jForm.append("id_annonce", id_annonce);
                jForm.append("id_illustration",id_ill_to_mod );
                jForm.append("NewIll", file);
                $.ajax({
                    url: url_update_illustration,
                    type: "POST",
                    data: jForm,
                    mimeType: "multipart/form-data",
                    contentType: false,
                    cache: false,
                    processData: false,
                    dataType:"json",
                    success: function(res) {
                        //write code here that you want to doing after successfully file upload.
                        if(res.status===true){
                            alert("Modification effectuee avec succes");
                            window.location.reload()
                        }
                        else{
                            alert("Erreur: "+ res.message ?? "Non identifiee")
                        }
                    },
                    error:function(err){
                        console.error(err);
                        alert("Une erreur est survenue!!")
                    }
                });
            }
        });

        $("#btnShowModalAdd").on("click",()=>{
            $("#formAddIll").get(0).reset()
        });

        $("#btnValidAdd").on("click",(e)=>{
            e.preventDefault();
            let file=$('#addNewIll').get(0)
            if(file!=null)file=file.files[0];
            if(file==null || file.length===0){
                alert("Illustration non renseigne!!!");
            }
            else{
                var jForm = new FormData();
                jForm.append("NewIll", file);
                jForm.append("id_annonce", id_annonce);
                $.ajax({
                    url: url_create_illustration,
                    type: "POST",
                    data: jForm,
                    dataType: "json",
                    mimeType: "multipart/form-data",
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(res) {
                        //write code here that you want to doing after successfully file upload.
                        if(res.status===true){
                            alert("Illustration enregistree avec succes!!!")
                            let image_info=res.data
                            $("#div_ill").append(
                                `<div class="col-md-12 col-lg-6 col-xl-4" id="ill`+image_info.id+`">
                                        <div class="card mb-2 bg-gradient-dark ">
                                            <img src="` + image_info.chemin_fichier+ `" alt="Illustration"/>
                                            <div class="d-flex"><!--card-img-overlay-->
                                                <button type="button" class="btn btn-danger btn-flat btn_delete_img" title="Supprimer" data-id="`+image_info.id+`"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </div>
                                </div>`
                            );
                            $("#modal_add_ill").modal("hide")
                            //window.location.reload()
                        }
                        else{
                            alert("Erreur :" +res.message??"Source Inconnue")
                        }
                    },
                    error:function(err){
                        console.error(err);
                        alert("Desole, une erreur est survenue. Reessayez plus tard!!!")
                    }
                });
            }
        });

        $("body").on("click",".btn_delete_img",(e)=>{
            let id=$(e.currentTarget).data("id");
            if(confirm("Etes-vous sur de vouloir supprimer cette illustration")){
                $.ajax({
                    url: url_delete_illustration+"?illustration_id="+id,
                    type: 'DELETE',
                    dataType: "json",
                    success: function(result) {
                        // Do something with the result
                        if(result.status){
                            alert("Suppression effectuee avec succes!!!")
                            $("#ill"+id).remove()
                            //window.location.reload()
                        }
                        else{
                            alert("Suppression non effectuee. Erreur :"+result.message);
                            console.error("Erreur ",result)
                        }
                    },
                    error:function(err){
                        alert("une erreur est survenue");
                        console.error("Erreur ",err)
                    }
                });
            }

        });

    </script>
</content>
</body>
</html>
