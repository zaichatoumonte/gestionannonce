<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="none"/>
    <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <asset:stylesheet src="header.css"/>
    <asset:stylesheet src="search_list.css"/>
</head>

<body>

<header class="site-header">
    <div class="wrapper site-header__wrapper">
        <a href="${createLink(url: "/")}" class="brand">LECOINCOIN</a>
        <nav class="nav">
            <ul class="nav__wrapper">
                <sec:ifLoggedIn>
                    <li class="nav__item"><span>Bonjour <sec:loggedInUserInfo field="username"/></span></li>
                    <li class="nav__item"><g:link controller="logout" action="index">Se deconnecter</g:link></li>
                </sec:ifLoggedIn>
                <sec:ifNotLoggedIn>
                    <li class="nav__item"><a href="${createLink(controller:'login')}">Se connecter</a></li>
                    <!--<li class="nav__item"><a href="#">S'inscrire</a></li>-->
                </sec:ifNotLoggedIn>
                <li class="nav__item">
                    <a href="${createLink(url: "/")}" class="">Accueil</a>
                </li>
            </ul>
        </nav>
    </div>
</header>

<div class="">

    <div class="table">
        <div class="table-header">
            <div class="header__item"><span id="name" class="filter__link" href="#">Titre</span></div>
            <div class="header__item"><span id="wins" class="filter__link filter__link--number" href="#">Description</span></div>
            <div class="header__item"><span id="draws" class="filter__link filter__link--number" href="#">Prix</span></div>
            <div class="illustrations__item"><span id="losses" class="filter__link filter__link--number" href="#">Illustrations</span></div>
            <div class="header__item"><span id="total" class="filter__link filter__link--number" href="#">Auteur</span></div>
        </div>
        <div class="table-content">
            <g:each in="${results}" var="res">
                <div class="table-row">
                        <div class="table-data">
                            <g:link controller="annonce" action="show" id="${res.id}">
                                ${res.titre}
                            </g:link>
                        </div>
                        <div class="table-data">${res.description}</div>
                        <div class="table-data">${res.prix}</div>
                        <div class="illustrations">
                            <g:each in="${res.illustrations}" var="illustration">
                                <img width="50px" height="50px" src="${grailsApplication.config.assets.url}${illustration.nomFichier}" />
                            </g:each>
                        </div>
                        <div class="table-data">
                            <g:link controller="user" action="show" id="${res.autheurId}">
                                ${fieldValue(bean: res, field: 'autheur.username')}
                            </g:link>
                        </div>
                </div>
            </g:each>
        </div>
    </div>
</div>

                    %{--<a href="#list-annonce" class="skip" tabindex="-1"><g:message code="default.link.skip.label"--}%
%{--                                                              default="Skip to content&hellip;"/></a>--}%

%{--<div class="nav" role="navigation">--}%
%{--    <ul>--}%
%{--        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--        <li><g:link class="create" action="create"><g:message code="default.new.label"--}%
%{--                                                              args="[entityName]"/></g:link></li>--}%
%{--    </ul>--}%
%{--</div>--}%

%{--<div id="list-annonce" class="content scaffold-list" role="main">--}%
%{--    <h1><g:message code="default.list.label" args="[entityName]"/></h1>--}%
%{--    <g:if test="${flash.message}">--}%
%{--        <div class="message" role="status">${flash.message}</div>--}%
%{--    </g:if>--}%

%{--    <table>--}%
%{--        <thead>--}%
%{--        <tr>--}%

%{--            <th class="sortable"><a href="/annonce/index?sort=title&amp;max=10&amp;order=asc">Title</a></th>--}%

%{--            <th class="sortable"><a href="/annonce/index?sort=description&amp;max=10&amp;order=asc">Description</a></th>--}%

%{--            <th class="sortable"><a href="/annonce/index?sort=price&amp;max=10&amp;order=asc">Price</a></th>--}%

%{--            <th class="sortable"><a href="/annonce/index?sort=illustrations&amp;max=10&amp;order=asc">Illustrations</a></th>--}%

%{--            <th class="sortable"><a href="/annonce/index?sort=author&amp;max=10&amp;order=asc">Author</a></th>--}%

%{--        </tr>--}%
%{--        </thead>--}%
%{--        <tbody>--}%

%{--        <g:each in="${annonceList}" var="annonce">--}%
%{--            <tr class="even">--}%
%{--                <td>--}%
%{--                    <g:link controller="annonce" action="show" id="${annonce.id}">--}%
%{--                        ${annonce.title}--}%
%{--                    </g:link>--}%
%{--                </td>--}%
%{--                <td>${annonce.description}</td>--}%
%{--                <td>${annonce.price}</td>--}%
%{--                <td>--}%
%{--                    <g:each in="${annonce.illustrations}" var="illustration">--}%
%{--                        <img src="${grailsApplication.config.assets.url}${illustration.filename}" />--}%
%{--                    </g:each>--}%
%{--                </td>--}%
%{--                <td>--}%
%{--                    <g:link controller="user" action="show" id="${annonce.author.id}">--}%
%{--                        ${fieldValue(bean: annonce, field: 'author.username')}--}%
%{--                    </g:link>--}%
%{--                </td>--}%
%{--            </tr>--}%
%{--        </g:each>--}%
%{--        </tbody>--}%
%{--    </table>--}%

%{--    <div class="pagination">--}%
%{--        <g:paginate total="${annonceCount ?: 0}"/>--}%
%{--    </div>--}%
%{--</div>--}%


</body>
</html>