<!doctype html>
<html>
<head>
    <title>Welcome to Grails</title>
    <asset:stylesheet src="header.css"/>
    <asset:stylesheet src="myStyle.css"/>
</head>
<body>
<header class="site-header">
    <div class="wrapper site-header__wrapper">
        <a href="${createLink(url: "/")}" class="brand">LECOINCOIN</a>
        <nav class="nav">
            <ul class="nav__wrapper">
                <sec:ifLoggedIn>
                    <li class="nav__item"><span>Bonjour <sec:loggedInUserInfo field="username"/></span></li>
                    <li class="nav__item"><g:link controller="logout" action="index">Se deconnecter</g:link></li>
                    <li class="nav__item"><g:link controller="annonces" action="userAuthentificatedAnnonces">Administration</g:link></li>
                </sec:ifLoggedIn>
                <sec:ifNotLoggedIn>
                    <li class="nav__item"><a href="${createLink(controller:'login')}">Se connecter</a></li>
                    <!--<li class="nav__item"><a href="#">S'inscrire</a></li>-->
                </sec:ifNotLoggedIn>
            </ul>
        </nav>
    </div>
</header>

<section class="image-section">
%{--    <asset:image src="pexels.jpg" class="image-landing" alt="image description"/>--}%
    <div class="s003">
            <g:form resource="${this.annonce}" method="GET" action="searchAnnonce" controller="index">
%{--            <form method="get" action="searchAnnonce" resource="${this.annonce}">--}%
            <div class="inner-form">
                <div class="input-field second-wrap">
                    <input id="search" type="text" placeholder="Chercher une annonce" name="mots_cles"/>
                </div>
                <div class="input-field third-wrap">
                    <button class="btn-search" type="submit">
                        <svg class="svg-inline--fa fa-search fa-w-16" aria-hidden="true" data-prefix="fas" data-icon="search" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                            <path fill="currentColor" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"></path>
                        </svg>
                    </button>
                </div>
            </div>
%{--        </form>--}%
            </g:form>
    </div>
</section>
<section class="list-annonces-container">
    <h2 class="last-annonces-title">Dernières annonces publiées</h2>
    <div class="grid">
        <g:each in="${data}" var="last_annonces">
            <div class="grid__item">
                <div class="card">
                    <img class="card__img" src="${grailsApplication.config.assets.url}${last_annonces.illustrations[0].nomFichier}" alt="Image" width="75px" height="75px">
                    <div class="card__content">
                        <h1 class="card__header">${last_annonces.titre}</h1>
                        <p class="card__text">
                            ${last_annonces.description}. <br>
                            ${last_annonces.prix}€
                        </p>
                        <g:link class="card__btn" controller="index" action="show" id="${last_annonces.id}">
                            Voir l'annonce<span>&rarr;</span>
                        </g:link>
                    </div>
                </div>
            </div>
        </g:each>
    </div>
   %{-- <div class="all-annonce">
        <p><a class="card__btn" href="${createLink(controller: 'Annonce', action: 'index')}">Voir toutes les annonces<span>&rarr;</span></a></p>
    </div>--}%
</section>
<footer>
    LECOINCOIN ALL RIGHT RESERVED
</footer>
</body>
</html>


%{--<!doctype html>--}%
%{--<html>--}%
%{--<head>--}%
%{--    <meta name="layout" content="main"/>--}%
%{--    <title>Welcome to Grails</title>--}%
%{--</head>--}%
%{--<body>--}%
%{--    <content tag="nav">--}%
%{--        <li class="dropdown">--}%
%{--            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Se connecter</a>--}%
%{--            <ul class="dropdown-menu">--}%
%{--                <li><a href="#">Controllers: ${grailsApplication.controllerClasses.size()}</a></li>--}%
%{--                <li><a href="#">Domains: ${grailsApplication.domainClasses.size()}</a></li>--}%
%{--                <li><a href="#">Services: ${grailsApplication.serviceClasses.size()}</a></li>--}%
%{--                <li><a href="#">Tag Libraries: ${grailsApplication.tagLibClasses.size()}</a></li>--}%
%{--            </ul>--}%
%{--        </li>--}%
%{--        <li class="dropdown">--}%
%{--            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">S'inscrire</a>--}%
%{--        </li>--}%
%{--    </content>--}%

%{--    <div class="svg" role="presentation">--}%
%{--        <div class="grails-logo-container">--}%
%{--            <asset:image src="grails-cupsonly-logo-white.svg" class="grails-logo"/>--}%
%{--        </div>--}%
%{--    </div>--}%

%{--    <div id="content" role="main">--}%
%{--        <section class="row colset-2-its">--}%
%{--            <h1>Welcome to Grails</h1>--}%

%{--            <p>--}%
%{--                Congratulations, you have successfully started your first Grails application! At the moment--}%
%{--                this is the default page, feel free to modify it to either redirect to a controller or display--}%
%{--                whatever content you may choose. Below is a list of controllers that are currently deployed in--}%
%{--                this application, click on each to execute its default action:--}%
%{--            </p>--}%

%{--            <div id="controllers" role="navigation">--}%
%{--                <h2>Available Controllers:</h2>--}%
%{--                <ul>--}%
%{--                    <g:each var="c" in="${grailsApplication.controllerClasses.sort { it.fullName } }">--}%
%{--                        <li class="controller">--}%
%{--                            <g:link controller="${c.logicalPropertyName}">${c.fullName}</g:link>--}%
%{--                        </li>--}%
%{--                    </g:each>--}%
%{--                </ul>--}%
%{--            </div>--}%
%{--        </section>--}%
%{--    </div>--}%

%{--</body>--}%
%{--</html>--}%
