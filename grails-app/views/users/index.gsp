<!doctype html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <title>Gestion des utilisateurs</title>
    <asset:stylesheet href="/datatables-bs4/css/dataTables.bootstrap4.min.css"/>
    <asset:stylesheet href="/datatables-responsive/css/responsive.bootstrap4.min.css"/>
    <asset:stylesheet href="/datatables-buttons/css/buttons.bootstrap4.min.css"/>
</head>
<body>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Utilisateur</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Administration</a></li>
                        <li class="breadcrumb-item active">Utilisateurs</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Liste des utilisateurs</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <g:each in="${userList}" var="user">
                                    <tr>
                                        <td>${user.username}</td>
                                        <td>${user.getAuthorities().isEmpty()?"":user.getAuthorities().first().authority}</td>
                                        <td>
                                            <button type="button" class="btn btn-outline-warning btn-flat btn_mod_user" title="Modifier" data-id="${user.id}" data-toggle="modal" data-target="#modal_mod"><i class="fa fa-edit"></i></button>
                                            <button type="button" class="btn btn-outline-danger btn-flat btn_sup_user" title="Supprimer le compte" data-id="${user.id}" ><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                </g:each>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <!-- /.col -->
            </div>
        </div>
    </section>
    <!-- /.content -->

    <div class="modal fade" id="modal_add">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Ajouter un utilisateur</h4>
                    <button type="button" class="close closeModalAdd" data-dismiss="modal" aria-label="Close" onclick="ExitAdding();">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form name="modAnnForm" id="addUserForm" method="post">
                        <p id="addErrorMessage" class="text-danger"></p>
                        <div class="form-group">
                            <label for="addUserName">Username</label>
                            <input type="text" class="form-control" name="addUserName" id="addUserName" placeholder="Nom d'utilisateur">
                        </div>
                        <div class="form-group">
                            <label for="addUserPass">Password</label>
                            <input type="password" class="form-control" name="addUserPass" id="addUserPass" placeholder="Mot de passe">
                        </div>
                        <div class="form-group">
                            <label for="addUserPassConf">Confirm Password</label>
                            <input type="password" class="form-control" name="addUserPassConf" id="addUserPassConf" placeholder="Mot de passe">
                        </div>
                        <div class="form-group">
                            <label for="addUserRole">Role</label>
                            <select class="form-control" id="addUserRole" name="addUserRole">
                                <g:each in="${Rolelist}" var="role">
                                    <option value="${role.id}">${role.authority}</option>
                                </g:each>
                            </select>
                        </div>
                    </form>

                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default closeModalAdd" data-dismiss="modal" onclick="ExitAdding();">Annuler</button>
                    <button type="button" class="btn btn-primary" id="btnUserVAlidAdd" form="addUserForm">Ajouter</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal_mod">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Modifier un utilisateur</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form name="modUserForm" id="modUserForm" method="post">
                        <p id="modErrorMessage" class="text-danger"></p>
                        <div class="form-group">
                            <label for="modUserName">Username *</label>
                            <input type="text" class="form-control" name="modUserName" id="modUserName" placeholder="Nom d'utilisateur">
                        </div>
                        <div class="form-group">
                            <label for="modUserRole">User Role *</label>
                            <select class="form-control" id="modUserRole" name="modUserRole">
                                <g:each in="${Rolelist}" var="role">
                                    <option value="${role.id}">${role.authority}</option>
                                </g:each>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="modUserPass">New Password</label>
                            <input type="password" class="form-control" name="modUserPass" id="modUserPass" placeholder="Nouveau Mot de passe">
                        </div>
                        <div class="form-group">
                            <label for="modUserPassConf">Confirm Password</label>
                            <input type="password" class="form-control" name="modUserPassConf" id="modUserPassConf" placeholder="Confirmer Mot de passe">
                        </div>
                    </form>

                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                    <button type="button" class="btn btn-primary" id="btnUserVAlidMod" form="modUserForm">Modifier</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<!-- /.content-wrapper -->
<content tag="javascript">
    <asset:javascript src="/datatables/jquery.dataTables.min.js"/>
    <asset:javascript src="/datatables-bs4/js/dataTables.bootstrap4.min.js"/>
    <asset:javascript src="/datatables-responsive/js/dataTables.responsive.min.js"/>
    <asset:javascript src="/datatables-responsive/js/responsive.bootstrap4.min.js"/>
    <asset:javascript src="/datatables-buttons/js/dataTables.buttons.min.js"/>
    <asset:javascript src="/datatables-buttons/js/buttons.bootstrap4.min.js"/>
    <asset:javascript src="/jszip/jszip.min.js"/>
    <asset:javascript src="/pdfmake/pdfmake.min.js"/>
    <asset:javascript src="/pdfmake/vfs_fonts.js"/>
    <asset:javascript src="/datatables-buttons/js/buttons.html5.min.js"/>
    <asset:javascript src="/datatables-buttons/js/buttons.print.min.js"/>
    <asset:javascript src="/datatables-buttons/js/buttons.colVis.min.js"/>

    <script>

        $(document).ready(function() {

            const url_get_user="<g:createLink controller="users" action="getUser"/>";
            const url_update_user="<g:createLink controller="users" action="updateUser"/>";
            const url_delete_user="<g:createLink controller="users" action="deleteUser"/>";
            const url_add_user="<g:createLink controller="users" action="createUser"/>";
            let stateAdding=false;
            let row_to_update;
            let idUserToUpdate;

            function ExitAdding(){
                stateAdding=false;
            }

            $(".closeModalAdd").on("click",()=>{
                ExitAdding();
            })

            let datatable=$('#example1').DataTable({
                dom: 'lrBftip',
                "responsive": true,
                buttons: [
                    {
                        class:"btn btn-info btn-flat",
                        text: '<i class="fa fa-plus"></i> Ajouter',
                        action: function ( e, dt, node, config ) {
                            if(
                                !(
                                    stateAdding &&
                                    confirm("Une action d'ajout debute n'a pas ete terminer. Voulez vous continuer cette action?")
                                ))
                            {
                                stateAdding=true
                                $("#addUserForm")[0].reset();
                            }
                            $("#addErrorMessage").text("");
                            $("#modal_add").modal("show");
                        }
                    },
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5',
                    "print",// "colvis"
                ],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
                }
            });
            datatable.buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

            $("#btnUserVAlidAdd").on("click",(e)=>{
                e.preventDefault();
                let username=$("#addUserName").val().trim();
                let pass=$("#addUserPass").val().trim();
                let passConf=$("#addUserPassConf").val().trim();
                let role_id=$("#addUserRole").val().trim();

                if(username=="" || pass=="" || role_id==null || passConf==""){
                    alert("Veuillez renseigner tous les champs svp!!!")
                    $("#addErrorMessage").text("Veuillez renseigner tous les champs svp!!!");
                }
                else if(passConf!=pass){
                    alert("Les mots de passe ne sont pas identiques")
                    $("#addErrorMessage").text("Les mots de passe ne sont pas identiques!!!");
                }
                else{
                    $("#addErrorMessage").text("");
                    $.ajax({
                        url: url_add_user,
                        type: 'POST',
                        data:{username,password:pass,role_id},
                        dataType: "json",
                        success: function(result) {
                            if(result.status){
                                alert("Utilisateur cree avec succes!!!")
                                ExitAdding();
                                //window.location.reload()
                                let user_data=result.data
                                datatable.row.add([
                                    user_data.username,
                                    user_data.role_name,
                                    '<button type="button" class="btn btn-outline-warning btn-flat btn_mod_user" title="Modifier le Role" data-id="'+user_data.id+'" data-toggle="modal" data-target="#modal_mod"><i class="fa fa-edit"></i></button>'+
                                    '<button type="button" class="btn btn-outline-danger btn-flat btn_sup_user" title="Supprimer le compte" data-id="'+user_data.id+'"><i class="fa fa-trash"></i></button>'
                                ]).draw();
                                $("#modal_add").modal("hide");

                            }
                            else{
                                $("#addErrorMessage").text(result.message);
                                alert(result.message);
                                console.error("Erreur ",result)
                            }
                        },
                        error:function(err){
                            $("#addErrorMessage").text("une erreur est survenue");
                            alert("une erreur est survenue");
                            console.error(err)
                        }
                    });
                }
            });

            $("table").on("click",".btn_mod_user",(e)=>{

                idUserToUpdate=$(e.currentTarget).data("id");
                row_to_update=$(e.currentTarget).parents("tr");
                $("#modErrorMessage").text("")
                $.get(url_get_user,{userId:idUserToUpdate}).done((res)=>{
                    if(res.status){
                        $("#modUserName").val(res.data.username);
                        $("#modUserRole").val(res.data.role_id);
                    }
                    else{
                        alert("Erreur Survenue : "+res.message);
                        console.error("Erreur:",res)
                    }
                }).fail((error)=>{
                    alert("Desole, une error est survenue lors du chargement des donnees!!!!");
                    console.error(error);
                })
            });

            $("#btnUserVAlidMod").on("click",(e)=>{
                e.preventDefault();
                let username=$("#modUserName").val().trim();
                let pass=$("#modUserPass").val().trim();
                let passConf=$("#modUserPassConf").val().trim();
                let role_id=$("#modUserRole").val();

                if(username=="" || role_id==null){
                    alert("Veuillez renseigner les champs obligatoire (*) svp!!!")
                    $("#modErrorMessage").text("Veuillez renseigner les champs obligatoire (*) svp!!!");
                }
                else if(pass != "" && passConf!=pass){
                    alert("Les mots de passe ne sont pas identiques")
                    $("#modErrorMessage").text("Les mots de passe ne sont pas identiques!!!");
                }
                else{
                    $("#modErrorMessage").text("");
                    $.ajax({
                        url: url_update_user,
                        type: 'PUT',
                        data:{id:idUserToUpdate,username,password:pass,id_role:role_id},
                        dataType: "json",
                        success: function(result) {
                            if(result.status){
                                alert("Modification effectuee avec succes!!!")
                                $("#modal_mod").modal("hide");
                                //window.location.reload()
                                let user_data=result.data
                                datatable.row(row_to_update).remove().draw();
                                datatable.row.add([
                                    user_data.username,
                                    user_data.role_name,
                                    '<button type="button" class="btn btn-outline-warning btn-flat btn_mod_user" title="Modifier" data-id="'+user_data.id+'" data-toggle="modal" data-target="#modal_mod"><i class="fa fa-edit"></i></button>'+
                                    '<button type="button" class="btn btn-outline-danger btn-flat btn_sup_user" title="Supprimer le compte" data-id="'+user_data.id+'"><i class="fa fa-trash"></i></button>'
                                ]).draw();
                                row_to_update=null;
                                idUserToUpdate=null;
                            }
                            else{
                                $("#modErrorMessage").text(result.message);
                                alert(result.message);
                                console.error("Erreur ",result)
                            }
                        },
                        error:function(err){
                            $("#modErrorMessage").text("une erreur est survenue");
                            alert("une erreur est survenue");
                            console.error(err)
                        }
                    });
                }
            });

            $("table").on("click",".btn_sup_user",(e)=>{
                let id=$(e.currentTarget).data("id");
                if(confirm("Etes-vous sur de vouloir supprimer cet utilisateur \n?")){
                    $.ajax({
                        url:  url_delete_user +"?user_id="+id,
                        type: 'DELETE',
                        dataType: "json",
                        success: function(result) {
                            // Do something with the result
                            if(result.status){
                                alert("Suppression effectuee avec succes!!!")
                                datatable.row($(e.currentTarget).parents("tr")).remove().draw()
                                //window.location.reload()
                            }
                            else{
                                alert("Suppression non effectuee. Erreur :"+result.message);
                                console.error("Erreur ",result)
                            }
                        },
                        error:function(err){
                            alert("une erreur est survenue");
                            console.error("Erreur ",err)
                        }
                    });
                }
            });
        });
    </script>
</content>
</body>
</html>
