package projetannonce

import grails.gorm.services.Service

@Service(Annonce)
interface AnnoncesService {

    Annonce get(Serializable id)

    List<Annonce> list(Map args)

    Long count()

    void delete(Serializable id)

    Annonce save(Annonce annonce)

}