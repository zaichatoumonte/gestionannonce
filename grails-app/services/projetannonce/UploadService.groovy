package projetannonce


import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

import grails.config.Config
import grails.core.support.GrailsConfigurationAware

class UploadService implements GrailsConfigurationAware{


    String cdnFolder
    String cdnRootUrl
    String uploadDirectory="Upload/"//System.getProperty("user.home")+"/Documents/Upload/"

    @Override
    void setConfiguration(Config co) {
        cdnFolder = co.getRequiredProperty('assets.path')
        cdnRootUrl = co.getRequiredProperty('assets.url')
        uploadDirectory=cdnFolder
    }

    def createUploadDirectory(){
        try{
            Path path = Paths.get(this.uploadDirectory)
            if (!Files.exists(path)) {
                Files.createDirectory(path);
            }
            return true;
        }
        catch(Exception e){
            return false
        }
    }

    def controlContentType(byte[] fileByte){
        def png =[137, 80, 78, 71] as byte[]
        def jpeg = [255, 216, 255, 224] as byte[]
        def jpeg2 =[255, 216, 255, 225 ] as byte[]

        def firstBytesFile=[fileByte[0],fileByte[1],fileByte[2],fileByte[3]] as byte[]

        if(firstBytesFile==png)return "png"
        else if(firstBytesFile == jpeg) return "jpg"
        else if(firstBytesFile == jpeg2) return "jpeg"

        throw new Exception("TYPE DE FICHIER NON SUPPORTE")

    }

    def doUpload(byte[] fileUploadBytes) {
        if(!createUploadDirectory()){
            throw new Exception("Impossible de creer le dossier qui contiendra le fichier a uploader")
        }
        String extension=this.controlContentType(fileUploadBytes)
        String fileName = UUID.randomUUID().toString()+"."+extension
        String fileAccess=this.uploadDirectory+fileName
        File fileToStore=new File(fileAccess);
        fileToStore.setBytes(fileUploadBytes);
        return fileName
    }
}
