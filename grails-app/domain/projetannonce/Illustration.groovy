package projetannonce

class Illustration {

    String nomFichier
    Integer tailleFichier

    static belongsTo = [annonce: Annonce]//depend de la classe annonce

    static constraints = {
        nomFichier nullable: false, blank: false
        tailleFichier nullable: true, min: 0//pourquoi nullable
    }

    boolean controlAnonceId(Long annonceId){
        return this.annonceId==annonceId;
    }
}
