package projetannonce

class Annonce {

    String titre
    String description
    Date dateCreated
    Date lastUpdated
    Float prix

    Integer getIllustrationCount () {
        illustrations?.size () ?: 0
    }

    String getDateCreatedFormat(){
        this.getDateCreated()?.format("dd-MM-yyyy")
    }

    String getDateUpdateFormat(){
        this.getLastUpdated()?.format("dd-MM-yyyy")
    }

    static belongsTo = [autheur: User]

    static hasMany = [illustrations: Illustration]

    static constraints = {
        titre nullable: false, blank: false, minSize: 10
        description nullable: false, blank: false
        prix nullable: false, min: 0F, scale: 2
    }

    static mapping = {
        description type: 'text'
    }
}

