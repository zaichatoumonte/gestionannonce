package projetannonce

class BootStrap {

    def init = { servletContext ->

        //Je crée les rôles que je vais utiliser dans mon application
        def adminRole = new Role(authority: "ROLE_ADMIN").save(flush: true)
        def modoRole = new Role(authority: "ROLE_MODO").save(flush: true)
        def userRole = new Role(authority: "ROLE_USER").save(flush: true)

        //Je crée l'utilisateur administrateur
        def adminUserInstance = new User(username: "admin", password: "admin").save(flush: true)

        //J'associe le role admin à l'utilisateur correspondant
        UserRole.create(adminUserInstance, adminRole, true)

        //Création de 5 utilisateurs
        ["Aicha", "Zatcha", "Most", "Moder", "Cliente"].each {
            String username ->
                def userInstance = new User(username: username, password: "password").save()
                //On ajoute 1 annonce par utilisateur
                def annonceIdx=1
                def annonceInstance =
                        new Annonce(titre: "Title Annonce $username",
                                description: "Description de l'annonce $username",
                                prix: 100 * annonceIdx)
                //On ajoute 5 illustrations par annonce
                (1..5).each {
                    Integer index->
                        def filename="prod-"+index+".jpg"
                        annonceInstance.addToIllustrations(new Illustration(nomFichier: "$filename", tailleFichier: 100))
                }
                userInstance.addToAnnonces(annonceInstance)
                userInstance.save()
        }
    }
    def destroy = {
    }
}
