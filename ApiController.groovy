package projetannonce

import grails.converters.JSON
import grails.converters.XML
import grails.plugin.springsecurity.annotation.Secured

@Secured('ROLE_ADMIN')
class ApiController {
    def passwordEncoder
    UploadService uploadService
//    Accessible sur /api/user/**
//    Gestion de : GET / PUT / PATCH / DELETE<
    def user()
    {
        switch (request.getMethod())
        {
            case "GET":
                /*
                * 0. on recupere l'utilsateur a afficher
                *   0.1 on renvoi 404 si non
                * 1. on l'affiche
                *  */
                if (!params.id)
                    respond([message: "Aucun paramètre fourni"], status: 400)
                def userInstance = User.get(params.id)
                if (!userInstance)
                    respond([message: "Aucun utilisateur trouvé"], status: 404)
                renderThis(userInstance, request.getHeader('Accept'))
                break;
            case "PUT":
                /*
                * 0. on recupere l'utilisateur concerné
                *   0.1 on renvoie 404 si non
                * 1. on verifie si toutes les données de la mise à jour ont été fournies
                *   1.1 on renvoie 400 si non
                * 2. on effectue la mise a jour - on verifie si le hash des mots de passe sont identiques si non, on remplace
                * 3. on renvoie 200 si tout est ok
                */
                if(!params.id)
                    respond([message: "Aucun paramètre fourni"], status: 400)
                User userInstance = User.get(params.id)
                if(!userInstance){
                    respond([message: "Aucun utilisateur trouvé"], status: 404)
                }
                Map data = request.JSON
                if(data.username == "" || data.password == ""){
                    respond([message: "Les données ne sont pas complètes"], status: 400)
                }
                if(!passwordEncoder.isPasswordValid(userInstance.password, data.password, null)){
                    userInstance.password = data.password
                }else{
                    respond([message: "Mot de passe identique"], status: 304)
                }
                userInstance.username = data.username
                userInstance.save(flush: true)
                respond([message: "Utilisateur modifié avec succès"], status: 200)
                break;
            case "PATCH":
                /*
                * 0. on recupere l'utilisateur concerné
                *   0.1 on renvoie 404 si non
                * 1. on verifie si toutes les données de la mise à jour ont été fournies
                *   1.1 on renvoie 400 si non
                * 2. on effectue la mise a jour - on verifie si le hash des mots de passe sont identiques si non, on remplace
                * 3. on renvoie 200 si tout est ok
                */
                if(!params.id)
                    respond([message: "Paramètre non fourni"], status: 400)
                User userInstance = User.get(params.id)
                if(!userInstance){
                    respond([message: "Utilisateur non trouvé"], status: 404)
                }
                Map data = request.JSON
                if(data.username != ""){
                    userInstance.username = data.username
                }
                if(data.password != ""){
                    if(!passwordEncoder.isPasswordValid(userInstance.password, data.password, null)){
                        userInstance.password = data.password
                    }
                }
                userInstance.save(flush: true)
                respond([message: "Utilisateur modifié avec succès"], status: 200)
                break;
            case "DELETE":
                /*
                * 0. on recupere l'utilisateur concerné
                *   0.1 on renvoie 404 si non
                * 1. on le supprime
                *   1.1 on renvoie 400 si non
                * 2. on renvoie 200 si tout est ok
                */
                if (!params.id)
                    respond([message: "Paramètre non fourni"], status: 400)
                User userInstance = User.get(params.id)
                if(!userInstance){
                    respond([message: "Utilisateur introuvable"], status: 404)
                }
                User.withTransaction {
                    UserRole.removeAll(userInstance)
                    userInstance.delete()
                }
                if(userInstance.delete()){
                    respond([message: "Erreur de suppression"], status: 400)
                }
                respond([message: "Utilisateur supprimé avec succès"], status: 200)
                break;
            default:
                return response.status = 405
                break;
        }
    }

//    Accessible sur /api/users/**
    def users()
    {
        switch (request.getMethod())
        {
            case "GET":
                // on affiche tous les users
                def usersInstance = User.getAll()
                renderThis(usersInstance, request.getHeader('Accept'))
                break;
            case "POST":
                /*
                0. verification des parametres : 400
                1. on crée un nouvel utilisateur
                    1.1 verifier que l'utilisateur se sauvegarde bien sinon 400
                2. on recupere le role
                    2.1 si le role n'existe pas 404
                3. associer le role a l'utilisateur
                    3.1 verifier que l'attribution s'est bien passée
                4. sauvegarde
                    4.1 sauvegarde echoue: 400
                   user.save(flush: true)
                5. créé : 200
                6. on recupere l'user créé
                                                                                */
                Map data = request.JSON
            // 0
                if(data.username == "" || data.password == "" || data.role == ""){
                    respond([message: "Paramètres incomplets"], status: 400)
                }
            // 1
                if(User.findByUsername((String)data.username)){
                    respond([message: "Utilisateur existant"], status: 404)
                }
            // 2
                Role role = Role.findByAuthority((String) data.role)
                if(!role){
                    respond([message: "role inexistant"], status: 404)
                }
                if(role){
                    def userInstance = new User(username:data.username, password: data.username)
                    if(!userInstance){
                        respond([message: "Erreur de création de l'utilisateur"], status: 400)
                    }
                    // 3
                    def createdRole = new UserRole(user: userInstance, role: role)
                    if(!createdRole){
                        respond([message: "Echec de création du role"], status: 400)
                    }
                    // 4
                    User a = userInstance.save(flush: true)
                    if(!a){
                        respond([message: "Erreur sauvegarde"], status: 400)
                    }
                }

                respond([message: "Création utilisateur OK"], status: 200)
                break;
            default:
                return response.status = 405
                break;
        }
    }

    def annonce()
    {
        switch (request.getMethod())
        {
            case "GET":
                /*
                * 0. on recupere l'annonce concernée
                *   0.1 on renvoie 404 si non
                * 1. on l'affiche
                * 2. on renvoie 200
                *
                */
                if (!params.id)
                    respond([message: "Aucun paramètre fourni"], status: 400)
                def annonceInstance = Annonce.get(params.id)
                if (!annonceInstance)
                    respond([message: "Aucune annonce trouvée"], status: 404)
                renderThis(annonceInstance, request.getHeader('Accept'))
                break;
            case "PUT":
                /*
                * 0. on recupere l'annonce concernée
                *   0.1 on renvoie 404 si non
                * 1. on verifie que les données de maj ont été fournies
                *   1.1 on renvoie 400 si non
                * 2. on fait la maj
                * 3. on renvoie 200
                */
                if(!params.id)
                    respond([message: "Aucun paramètre fourni"], status: 400)
                Annonce annonceInstance = Annonce.get(params.id)
                if(!annonceInstance){
                    respond([message: "L'annonce n'existe pas"], status: 404)
                }
                Map data = request.JSON
                if(data.title == "" || data.description == "" || (Float)data.price < 0){
                    respond([message: "Aucunes données fournies"], status: 400)
                }
                annonceInstance.titre = data.title
                annonceInstance.description = data.description
                annonceInstance.prix = (Float) data.price
                annonceInstance.save(flush: true)
                respond([message: "Mise à jour effectuée"], status: 200)
                break;
            case "PATCH":
                /*
                * 0. on recupere l'annonce concernée
                *   0.1 on renvoie 404 si non
                * 1. on verifie que les données de maj ont été fournies
                * 2. on fait les maj
                *   2.1 on verifie que les illustrations fournies existent bien sinon 4
                * 3. on renvoie 200
                */
                if(!params.id)
                    respond([message: "Aucun paramètre fourni"], status: 400)
                Annonce annonceInstance = Annonce.get(params.id)
                if(!annonceInstance){
                    respond([message: "L'annonce n'a pas été trouvée"], status: 404)
                }
                Map data = request.JSON
                if(data.title != ""){
                    annonceInstance.titre = data.title
                }
                if(data.description != ""){
                    annonceInstance.description = data.description
                }
                if(data.price != ""){
                    annonceInstance.prix = (Float)data.price
                }
                if(!data.illustrations.isEmpty()){
                    data.illustrations.each{
                        Map obj ->
                            def illus = Illustration.get(obj.id)
                            if(!illus)
                                respond([message: "Illustration inexistante"], status: 404)
                            illus.annonce = annonceInstance
                    }
                }
                annonceInstance.save(flush: true)
                respond([message: "Mise à jour effectuée"], status: 200)
                break;
            case "DELETE":
                /*
                * 0. on recupere l'annonce concernée
                *   0.1 on renvoie 404 si non
                * 1. on le supprime
                *   1.1 on renvoie 400 si non
                * 2. on renvoie 200 si tout est ok
                */
                if (!params.id)
                    respond([message: "Aucun paramètre fourni"], status: 400)
                Annonce annonceInstance = Annonce.get(params.id)
                if(!annonceInstance){
                    respond([message: "Aucune annonce trouvée"], status: 404)
                }
                Annonce.withTransaction {
                    annonceInstance.delete()
                }
                if(annonceInstance.delete()){
                    respond([message: "Erreur de suppression"], status: 400)
                }
                respond([message: "Annonce supprimée"], status: 200)
                break;
            default:
                return response.status = 405
                break;
        }
    }

    def annonces()
    {
        switch (request.getMethod())
        {
            case "GET":
                def annonceInstance = Annonce.getAll()
                renderThis(annonceInstance, request.getHeader('Accept'))
                break;
            case "POST":
            /*
                envoyer de la data:
                1)convertir la donnee binaire base64 (augmentation d'environ 30%)
                2)passer par un envoi en multipart/form-data au lieu d'envoyer la donnee en JSON (ne fonctionne qu'en POST)

                quand envoyer de la data :
                1) Envoyer les données de la / des images en meme temps que les données de l'annonce
                2) Créer l'annonce sans illustration et passer par l'update d'aonnonce pour ajouter les illustrations
            */
                // on recupere l'image
                def dataImage = request.getFile('image')
                if(dataImage==null){
                    respond([message: "Image non fournie"], status: 404)
                }
                // on recupere la taille et le repertoire du fichier
                def imageBytes = dataImage.getBytes()
                def imageSize = dataImage.getSize()
                def path = uploadService.doUpload(imageBytes)
                // on sauvegarde l'annonce et l'illustration
                // data.title == "" || data.description == "" || (Float)data.price < 0
                if(params.author == ""){
                    respond([message: "Auteur non précisé"], status: 400)
                }
                def user = User.findById(params.author)
                if(!user){
                    respond([message: "Auteur inexistant"], status: 404)
                }

                if(params.title == "" || params.description == "" || Float.parseFloat(params.price) < 0){
                    respond([message: "Paramètre non fournis"], status: 400)
                }
                Annonce.withTransaction {
                    Annonce annonce = new Annonce(titre: params.title, description: params.description, prix: Float.parseFloat(params.price))
                    Illustration illustration = new Illustration(tailleFichier: imageSize, nomFichier: path)
                    user.addToAnnonces(annonce)
                    user.save()
                    annonce.addToIllustrations(illustration)
                    annonce.save()
                    println(params)
                }
                respond([message: "Annonce crée"], status: 200)
                break;
            default:
                return response.status = 405
                break;
        }
    }


    def illustration()
    {
        switch (request.getMethod())
        {
            case "GET":
                /*
                * 0. on recupere l'illustration concernée
                *   0.1 on renvoie 404 si non
                * 1. on l'affiche
                * 2. on renvoie 200
                *
                */
                if (!params.id)
                    respond([message: "Aucun paramètre fourni"], status: 400)
                def illustrationInstance = Illustration.get(params.id)
                if (!illustrationInstance)
                    respond([message: "Aucune illustration trouvée"], status: 404)
                renderThis(illustrationInstance, request.getHeader('Accept'))
                break;
            case "PUT":
                /*
                * 0. on recupere l'illustration concernée
                *   0.1 on renvoie 404 si non
                * 1. on verifie que les données de maj ont été fournies
                *   1.1 on renvoie 400 si non
                * 2. on fait la maj
                * 3. on renvoie 200
                */
                if(!params.id)
                    respond([message: "Aucun paramètre fourni"], status: 400)
                Illustration illustrationInstance = Illustration.get(params.id)
                if(!illustrationInstance){
                    respond([message: "L'illustration n'existe pas"], status: 404)
                }
                Map data = request.JSON
                if(data.size <= 0 || data.filename == ""){
                    respond([message: "Aucunes données fournies"], status: 400)
                }
                illustrationInstance.tailleFichier = (Integer) data.size
                illustrationInstance.nomFichier = data.filename
                illustrationInstance.save(flush: true)
                respond([message: "Mise à jour effectuée"], status: 200)
                break;
            case "PATCH":
                /*
                * 0. on recupere l'annonce concernée
                *   0.1 on renvoie 404 si non
                * 1. on verifie que les données de maj ont été fournies
                * 2. on fait les maj
                *   2.1 on verifie que les illustrations fournies existent bien sinon 4
                * 3. on renvoie 200
                */
                if(!params.id)
                    respond([message: "Aucun paramètre fourni"], status: 400)
                Illustration illustrationInstance = Illustration.get(params.id)
                if(!illustrationInstance){
                    respond([message: "L'illustration n'a pas été trouvée"], status: 404)
                }
                Map data = request.JSON
                if(data.size > 0){
                    illustrationInstance.tailleFichier = (Integer) data.size
                }
                if(data.filename != ""){
                    illustrationInstance.nomFichier = data.filename
                }
                illustrationInstance.save(flush: true)
                respond([message: "Mise à jour effectuée"], status: 200)
                break;
            case "DELETE":
                /*
                * 0. on recupere l'annonce concernée
                *   0.1 on renvoie 404 si non
                * 1. on le supprime
                *   1.1 on renvoie 400 si non
                * 2. on renvoie 200 si tout est ok
                */
                if (!params.id)
                    respond([message: "Aucun paramètre fourni"], status: 400)
                Illustration illustrationInstance = Illustration.get(params.id)
                if(!illustrationInstance){
                    respond([message: "Aucune illustration trouvée"], status: 404)
                }
                Annonce.withTransaction {
                    illustrationInstance.delete()
                }
                if(illustrationInstance.delete()){
                    respond([message: "Erreur de suppression"], status: 400)
                }
                respond([message: "illustration supprimée"], status: 200)
                break;
            default:
                return response.status = 405
                break;
        }
    }

    def illustrations()
    {
        switch (request.getMethod())
        {
            case "GET":
                def illustrationInstance = Illustration.getAll()
                renderThis(illustrationInstance, request.getHeader('Accept'))
                break;
            case "POST":
                /*
                    envoyer de la data:
                    1)convertir la donnee binaire base64 (augmentation d'environ 30%)
                    2)passer par un envoi en multipart/form-data au lieu d'envoyer la donnee en JSON (ne fonctionne qu'en POST)

                    quand envoyer de la data :
                    1) Envoyer les données de la / des images en meme temps que les données de l'annonce
                    2) Créer l'annonce sans illustration et passer par l'update d'aonnonce pour ajouter les illustrations
                */
                // on recupere l'image
                def dataImage = request.getFile('image')
                if(dataImage==null){
                    respond([message: "Image non fournie"], status: 404)
                }
                // on recupere la taille et le repertoire du fichier
                def imageBytes = dataImage.getBytes()
                def imageSize = dataImage.getSize()
                def path = uploadService.doUpload(imageBytes)
                // on sauvegarde l'annonce et l'illustration
                // data.title == "" || data.description == "" || (Float)data.price < 0
                if(params.author == ""){
                    respond([message: "Auteur non précisé"], status: 400)
                }
                def user = User.findById(params.author)
                if(!user){
                    respond([message: "Auteur inexistant"], status: 404)
                }

                if(params.title == "" || params.description == "" || Float.parseFloat(params.price) < 0){
                    respond([message: "Paramètre non fournis"], status: 400)
                }
                Annonce.withTransaction {
                    Annonce annonce = new Annonce(titre: params.title, description: params.description, prix: Float.parseFloat(params.price))
                    Illustration illustration = new Illustration(tailleFichier: imageSize, nomFichier: path)
                    user.addToAnnonces(annonce)
                    user.save()
                    annonce.addToIllustrations(illustration)
                    annonce.save()
                    println(params)
                }
                respond([message: "Annonce crée"], status: 200)
                break;
            default:
                return response.status = 405
                break;
        }
    }

    def renderThis(Object instance, String acceptHeader)
    {
        switch (acceptHeader)
        {
            case "json":
            case "application/json":
            case "text/json":
                render instance as JSON
                return
                break;
            case "xml":
            case "application/xml":
            case "text/xml":
                render instance as XML
                return
                break;
            default:
                return response.status = 406
                break;
        }
    }
}

